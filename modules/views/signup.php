<?php 

	$title = "Sign Up";
	require_once '../../config/config.php';
	include ROOT . 'modules/templates/header.php'; 
	include ROOT . 'config/database.php';  

	$data 	= new Database();
	$mysqli = $data->conn; 

?>


<div class="container" id="account" style="margin-top: 120px">
	<div class="row">

		<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
			<div class="panel" >
				<div class="panel-heading" style="background-color: crimson;">
					<div class="panel-title" style="color: #fff;" >Sign Up</div>
					<!-- <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div> -->
				</div>     

				<div style="padding-top:30px" class="panel-body signupForm">

					<form id="signup" name="signup-form">

						<div id="output"></div>

						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="fa fa-user"></i></span>
							<input id="fullname" type="text" class="form-control" name="fullname" value="" placeholder="Full Name">                                        
						</div>

						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
							<input id="email" type="email" class="form-control" name="email" value="" placeholder="Email">                                        
						</div>

						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="fa fa-lock"></i></span>
							<input id="password" type="password" class="form-control" name="password" placeholder="Password">
						</div>

						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<input id="dob" type="text" class="form-control" name="dob" placeholder="Date Of Birth">
						</div>


						<div class="input-group">
							<div class="checkbox">
								<label>
									<input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
								</label>
							</div>
						</div>


						<div style="margin-top:10px" class="form-group">
							<!-- Button -->
							<div class="col-sm-12 controls">
								<button id="signupBtn" class="btn btn-success" name="signupBtn">Register</button>
							</div>
						</div>

						
						<div class="form-group" >
							<div class="col-md-12 control">
								<div style="border-top: 1px solid#888; padding-top:15px; font-size:85%; margin-top: 25px">
									Existing account? 
									<a href="login.php" >
										Log In!
									</a>
								</div>
							</div>
						</div>

					</form>    

				</div>                     
			</div>  
		</div>

	</div>
</div>



<?php include ROOT . 'modules/templates/footer.php'; ?>

<script>

	$(function(){
		$("#dob").datepicker();
	});

	$(document).ready(function(){

		// $(document).off("click", "#signupBtn").on("click", "#signupBtn", function(e){
		// 	e.preventDefault();

		//     $("form[name='signup-form']").validate({
		//       // Specify validation rules
		//       rules: {
		       
		//         fullname: {
		//           required: true,
		//         },
		//         email: {
		//           required: true,
		//           email: true
		//         },
		//         password: {
		//           required: true,
		//         },
		//         dob: {
		//           required: true,
		//         },
		//       },
		//       // Specify validation error messages
		//       messages: {
		        
		//         fullname: {
		//           required: "Please enter your full name",
		//         },
		//         email: {
		//           required: "Please enter email" ,
		//           email: "Please enter a valid email address"
		//         },
		//         password: {
		//           required: "Please enter password",
		//         },
		//         dob: {
		//           required: "Please select your date of birth",
		//         },
		//       },
		       
		//     });

  // 		});


		$("#signupBtn").on("click", function(e){
			e.preventDefault();

			var fullname 	= $("#fullname").val();
			var	email 		= $("#email").val();
			var password 	= $("#password").val();
			var dob 		= $("#dob").val();

			var data = "fullname=" + fullname + "&email=" + email + "&password=" + password + "&dob=" + dob;  

			//alert(data); 

			$.ajax({
				type: "post",
				url: "../control/signupaction.php?",
				data: data,
				success: function(response) {
					$("#output").html(response);
				}
			});
		});

	});

</script>