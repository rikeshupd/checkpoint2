<?php 

$title = "AngularJS";
require_once '../../config/config.php';
include ROOT . 'modules/templates/header.php'; 
include ROOT . 'config/database.php';  

$data 	= new Database();
$mysqli = $data->conn; 

?>

<div class="container" id="angularform" style="margin-top: 120px">
    <div class="row">
        <div class="col-md-12" ng-app="myapp" ng-controller="usercontroller" ng-init="displayData()">
            <h2 style="text-align: center;">AngularJS Form</h2>
            <hr>
            <form name="angularForm">
                <div class="control-group">
                    <div class="form-group ">
                        <label><i class="fa fa-user"></i> Name</label>
                        <input type="text" class="form-control" placeholder="Name" name="name" id="name" ng-model="name">  
                    </div>
                </div>

                <div class="control-group">
                    <div class="form-group ">
                        <label><i class="fa fa-users"></i> Group Name</label>
                        <input type="text" class="form-control" placeholder="Group Name" name="groupname" id="groupname" ng-model="groupname">  
                    </div>
                </div>

                <div class="control-group">
                    <div class="form-group ">
                        <label><i class="fa fa-wrench"></i> Specialization Number</label>
                        <input type="text" class="form-control" placeholder="Specialization Number" name="number" id="number" ng-model="number">  
                    </div>
                </div>

                <br>

                <div class="control-group">
                    <div class="form-group " style="text-align: center;">
                        <input type="submit" class="btn btn-primary form-control" name="submit" id="submit" value="UPLOAD" ng-click="insertData()" style="background-color: crimson; width:50%;">
                    </div>
                </div>


                <div class="panel panel-info" style="margin-top: 50px">
                    <div class="panel-heading" id="panelHead" style="background-color: crimson; color:#fff;">Click to show Data Table <i class="fa fa-arrow-down"></i></div>
                    <div class="panel-body" id="panelContent">

                        <table class="table table-responsive">
                            <tr>
                                <th>Name</th>
                                <th>Group Name</th>
                                <th>Specialization Number</th>
                            </tr>

                            <tr ng-repeat="x in info">
                                <td>{{x.name | capitalize}}</td>
                                <td>{{x.groupname | capitalize}}</td>
                                <td>{{x.number}}</td>
                            </tr>
                        </table>

                    </div>
                </div>

            </form>

        </div>

    </div> <!-- /.ROW -->
</div>  <!-- /.CONTAINER -->






<?php include ROOT . 'modules/templates/footer.php'; ?>


<script>

    $(document).ready(function(){

        $(document).off("click", "#submit").on("click", "#submit", function(){

            $("form[name='angularForm']").validate({
                // Specify validation rules
                rules: {

                    name: {
                        required: true,
                    },
                    groupname:{
                        required: true,
                    },
                    number: {
                        required: true
                    }
                },
                // Specify validation error messages
                messages: {

                    name: {
                        required: "Please enter Name",
                    },
                    groupname: {
                        required: "Please enter Group Name",
                    },
                    number: {
                        required: "Please enter Specialization number" 
                    }
                },

            });

        });
    });
</script>


<script>
    var app = angular.module("myapp",[]);

    app.controller("usercontroller", function($scope, $http){
        $scope.insertData = function() {
            $http.post (
                "../control/angularinsert.php",
                {'name': $scope.name, 'groupname': $scope.groupname, 'number': $scope.number}
            ).success(function(data){
                swal({
                    title: data,
                    icon: "success",
                    button: "Ok!",
                });
                $scope.name = null;
                $scope.groupname = null;
                $scope.number = null;
                $scope.displayData();
            });
        }

        $scope.displayData = function() {
            $http.get ("../control/angulardisplay.php")
                .success(function(data){
                $scope.info = data;
            });
        }
    });

    app.filter('capitalize', function() {
        return function(input) {
            return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
        }
    });

</script>


<script>


    $(document).ready(function(){
        $("#panelHead").on("click", function(){
            $("#panelContent").slideToggle(1000);
        }); 
    });


</script>