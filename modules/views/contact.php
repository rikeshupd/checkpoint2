<?php 
	$title = "Contact";
	require_once '../../config/config.php';
	include ROOT . 'modules/templates/header.php'; 
	include ROOT . 'config/database.php';  

	$data 	= new Database();
	$db 	= $data->conn;     
?>

<h2 style="text-align: center; margin-top:150px;">CONTACT</h2>
<hr>
<div class="container">
	<div class="row">
		
		<div id="map"></div>

		<script>
		    function initMap() {
		        var uluru = {lat: 27.692134, lng: 85.319518};
		        var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 14,
					center: uluru
		        });
		        var marker = new google.maps.Marker({
					position: uluru,
					map: map
		        });
		    }
		</script>

		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATQ1kRnLN9LU0ajAZJbCrBmakbhruQvas&callback=initMap">
		</script>	

	</div>
</div>


<div class="container" style="margin-top: 50px">
	<div class="row" style="text-align: center;">
		<strong><h2>GET IN TOUCH WITH US</h2></strong>
		<h4>Fill out the form below to send us a message and we will get back to you as soon as possible!</h4>
	</div>
</div>


<div class="container" style="margin-top: 50px">
	<div class="row" style="text-align: center;">
		<div class="col-md-5">
			<h3>Our Location</h3>
			<p>The Trade Tower</p>
			<p>Thapathali, Kathmandu</p>
			<p>Phone no: +977 999999999</p>
		</div>

		<div class="col-md-5">
			<h3>Contact Us</h3>
			<div style="margin-bottom: 25px" class="input-group">
				<span class="input-group-addon"><i class="fa fa-user"></i></span>
				<input id="fullname" type="text" class="form-control" name="fullname" value="" placeholder="Full Name">                                        
			</div>

			<div style="margin-bottom: 25px" class="input-group">
				<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
				<input id="email" type="email" class="form-control" name="email" value="" placeholder="Email">                                        
			</div>

			<div style="margin-bottom: 25px" class="input-group">
				<span class="input-group-addon"><i class="fa fa-pencil"></i></span>
				<input id="subject" type="text" class="form-control" name="subject" placeholder="Subject">
			</div>

			<div style="margin-bottom: 25px" class="input-group">
				<span class="input-group-addon"><i class="fa fa-pencil"></i></span>
				<textarea id="message" type="text" class="form-control" name="message" placeholder="Write your message here..."></textarea>
			</div>

		</div>
	</div>
</div>





<?php include ROOT . 'modules/templates/footer.php'; ?>

