<?php 
	$title = "Home";
	require_once '../../config/config.php';
	include ROOT . 'modules/templates/header.php'; 
	include ROOT . 'config/database.php';  

	$data 	= new Database();
	$mysqli = $data->conn;     
?>


<div class="container" id="account" style="margin-top: 120px">
	<div class="row">

		<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
			<div class="panel panel-info" >
				<div class="panel-heading" style="background-color: crimson;" >
					<div class="panel-title" style="color:#fff;">Login</div>
<!--					<div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div>-->
				</div>     

				<div style="padding-top:30px" class="panel-body" >

					<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

					<form name="loginform" method="post" action="../control/loginaction.php">

						<?php if (isset($_SESSION['logout_success'])): ?>
			                <div class="alert alert-success" style="font-size: 15px; text-align: center;">
			                  <i class="fa fa-check-circle"></i>
			                  <?= $_SESSION['logout_success'] ?>
			                  <?php unset($_SESSION['logout_success']) ?>
			                </div>
			             <?php endif; ?>

			             <?php if (isset($_SESSION['login_fail'])): ?>
			                <div class="alert alert-danger" style="font-size: 15px; text-align: center;">
			                  <i class="fa fa-times-circle"></i>
			                  <?= $_SESSION['login_fail'] ?>
			                  <?php unset($_SESSION['login_fail']) ?>
			                </div>
			             <?php endif; ?>

						<div id="output"></div>

						<div class="control-group">
							<div class="form-group">
								<label>Email</label>
								<input type="text" class="form-control" placeholder="Enter your email" name="email" id="email">  
							</div>
				        </div>

						<div class="control-group">
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control" placeholder="Enter your password" name="password" id="password">  
							</div>
				        </div>


						<div style="margin-top:10px" class="form-group">
							<!-- Button -->

							<div class="controls">
								<button id="loginBtn" class="btn btn-success" name="loginBtn">Login</button>
								<a id="btn-fblogin" href="#" class="btn btn-primary">Login with Facebook</a>

							</div>
						</div>


						<div class="form-group">
							<div class="col-md-12 control">
								<div style="border-top: 1px solid#888; padding-top:15px; font-size:85%; margin-top: 25px" >
									Don't have an account? 
									<a href="signup.php">
										Sign Up Here
									</a>
								</div>
							</div>
						</div>    
					</form>    

				</div>                     
			</div>  
		</div>

	</div>
</div>



<?php include ROOT . 'modules/templates/footer.php'; ?>

<script>

	$(document).ready(function(){

		$(document).off("click", "#loginBtn").on("click", "#loginBtn", function(){
			// alert('hi');
			// e.preventDefault();

		    $("form[name='loginform']").validate({
		      // Specify validation rules
		      rules: {
		       
		        email: {
		          required: true,
		          email: true
		        },
		        password: {
		          required: true,
		        },		       
		      },
		      // Specify validation error messages
		      messages: {
		        
		        email: {
		          required: "Please enter email",
		          email: "Please enter a valid email address"
		        },
		        password: {
		          required: "Please enter password",
		        },
		      },
		       
		    });

  		});



	});

</script>