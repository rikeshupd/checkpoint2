<?php 
$title = "Contact";
require_once '../../config/config.php';
include ROOT . 'modules/templates/header.php'; 
include ROOT . 'config/database.php';  

$data 	= new Database();
$db 	= $data->conn;     
?>


<div class="container"  style="margin-top: 53px">
    <div class="row" style="margin-top: 50px">

        <div class="col-md-12" >
            <h2 style="text-align: center;">JSON/XML</h2>
            <hr>

            <?php
            $sql = $data->select("*", "`tbl_json_xml`");
            $result = $db->query($sql);
            $count = mysqli_num_rows($result);
            ?>

            <div id="json-table" style="overflow-x: scroll;">
                <table class="table table-responsive" style="margin-top: 25px;">
                    <tr>
                        <th>ID</th>
                        <th>Country</th>
                        <th>Capital</th>
                        <th>Longitude</th>
                        <th>Latitude</th>
                        <th>Currency</th>
                    </tr>
                    <?php 
                    if($count > 0) :
                    while ($res = $result->fetch_array()) :			 
                    ?>

                    <tr>
                        <td nowrap><?php echo $res['id']?></td>
                        <td nowrap><?php echo $res['country']?></td>
                        <td nowrap><?php echo $res['capital']?></td>
                        <td nowrap><?php echo $res['longitude']?></td>
                        <td nowrap><?php echo $res['latitude']?></td>
                        <td nowrap><?php echo $res['currency']?></td>
                    </tr>

                    <?php endwhile;?>
                    <?php else: ?>
                    <tr style="text-align: center;" >
                        <td colspan ="5">No Data Found.</td>
                    </tr>
                    <?php endif; ?>	
                    <h4>Links to JSON and XML formats: </h4>
                    <a href="../control/jsonformat.php"><button class="btn btn-info">{ ; } JSON Format</button></a>&emsp;&emsp;
                    <a href="../control/xmlformat.php"><button class="btn btn-warning">.XML Format</button></a>
                </table>
            </div>


        </div>
    </div>
</div>



<?php include ROOT . 'modules/templates/footer.php'; ?>
