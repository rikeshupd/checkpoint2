<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<?php
session_start();

require_once '../../config/config.php';
include ROOT . "config/database.php";
$data = new Database();
$mysqli = $data->conn;

if (isset($_POST['loginBtn'])) {

	$email = mysqli_real_escape_string($data->conn, $_POST['email']);
	$password = mysqli_real_escape_string($data->conn, sha1($_POST['password']));

	// if (empty($email) || empty($password)) {
	// 	echo '<div class="alert alert-danger" style="font-size: 15px; text-align: center;">
	//    			<i class="fa fa-times-circle"></i> A field is empty. Please enter all field.
	//       	</div>';

	// } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	// 	echo '<div class="alert alert-danger" style="font-size: 15px; text-align: center;">
	//    			<i class="fa fa-times-circle"></i> Invalid email format.
	//       	</div>';

	// } else {
	
		$sql = $data->select("*", "`tbl_user_detail`", "`email` = ? AND `password` = ?"); 
		//$result = $mysqli->query($sql);

		$stmt = $mysqli->prepare($sql);
	    $stmt->bind_param('ss', $email, $password);
	    $stmt->execute();

	    $result = $stmt->get_result();

		if ($row = $result->fetch_assoc()) {
			
			
			$_SESSION["userid"] = $row['id']; //session set for profile
			$_SESSION["email"] 	= $row['email'];

			$_SESSION['welcome'] = "Welcome " . ucwords($row['fullname']);
			header('Location:../../index.php');  //header

			// echo '<div class="alert alert-success" style="font-size: 15px; text-align: center;">
		 //   			<i class="fa fa-check-circle"></i> Logged In Successfully 
		 //      	</div>';

			// echo '<script> 
			// 		swal({
			// 		  title: "Logged in successfully!",
			// 		  icon: "success",
			// 		  button: "Ok!",
			// 		});
			// 	</script>';
			
			
		} else {

			// echo '<div class="alert alert-danger" style="font-size: 15px; text-align: center;">
		 //   			<i class="fa fa-times-circle"></i> Email or password incorrect.
		 //      	</div>';
			$_SESSION['login_fail'] = "Username or password incorrect";
			header("Location: ../views/login.php");
		}

	//}

	
  }  

?>