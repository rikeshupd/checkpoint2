<?php 
	require_once '../../config/config.php';
	include ROOT . 'config/database.php';  

	$data 	= new Database();
	$db 	= $data->conn;     
?>

<!DOCTYPE html>
<html>
<head>
	<title>JSON Format</title>
	<!-- British College Icon -->
    <link rel="icon" href="../../public/image/icon/lbu_FLu_icon.ico">
</head>
<body>

<?php 

    $sql = $data->select("*", "`tbl_json_xml`");
    $result = $db->query($sql);
    
    $json_array = array();

    while ($row = $result->fetch_assoc()) {
    	$json_array[] = $row;
    }

    echo json_encode($json_array);

?>

</body>
</html>