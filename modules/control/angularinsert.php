<?php
session_start();

require_once '../../config/config.php';
include ROOT . "config/database.php";
$data 	= new Database();
$mysqli = $data->conn;

$input = json_decode(file_get_contents("php://input"));


if(count($input) > 0) {
	$name = mysqli_real_escape_string($data->conn, $input->name);
	$groupname 	= mysqli_real_escape_string($data->conn, $input->groupname);
	$number 	= mysqli_real_escape_string($data->conn, $input->number);

	$insert_data = array(
		"name" 	=> $name,
		"groupname"		=> $groupname,
		"number"		=> $number,
	);

	$result = $data->insert('tbl_angular', $insert_data);

	if ($result) {
		echo "Data inserted successfully!";
	} else {
		echo "Data insert failed";
	}
}

	
    

?>