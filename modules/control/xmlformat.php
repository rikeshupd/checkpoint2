<?php 
	require_once '../../config/config.php';
	include ROOT . 'config/database.php';  

	$data 	= new Database();
	$db 	= $data->conn; 

	$sql 	= $data->select("*", "`tbl_json_xml`");
    $result = $db->query($sql);

    $xml = new DOMDocument("1.0", "UTF-8");
    $xml->formatOutput = true;

    //creating an xslt adding processing line
    $xslt = $xml->createProcessingInstruction('xml-stylesheet', 'type="text/xsl" href="xmlreport.xsl"');

    //adding it to the xml
    $xml->appendChild($xslt);

    $countries = $xml->createElement("countries");
    $xml->appendChild($countries);

    while ($row = $result->fetch_array()) {
        
    	$country = $xml->createElement("country");
    	$countries->appendChild($country);

    	$name = $xml->createElement("name", $row['country']);
    	$country->appendChild($name);

    	$capital = $xml->createElement("capital", $row['capital']);
    	$country->appendChild($capital);

    	$longitude = $xml->createElement("longitude", $row['longitude']);
    	$country->appendChild($longitude);

    	$latitude = $xml->createElement("latitude", $row['latitude']);
    	$country->appendChild($latitude);

    	$currency = $xml->createElement("currency", $row['currency']);
    	$country->appendChild($currency);
    }

    echo "<xmp>".$xml->saveXML()."</xmp>";

    $xml->save("xmlreport.xml");

?>

<html>
<head>
	<title>XML Format</title>
	<!-- British College Icon -->
    <link rel="icon" type="image/gif/png" href="<?= "../../public/image/icon/logo.png" ?>">
</head>
<body>

	<a href="xmlreport.xml"><button style="background-color: black; color: white; cursor: pointer;"><strong>Click here to view output of this XML format using XSLT stylesheet</strong></button></a>

</body>
</html>

