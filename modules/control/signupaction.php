<?php
session_start();

require_once '../../config/config.php';
include ROOT . "config/database.php";
$data = new Database();
$mysqli = $data->conn;

	$fullname 	= $_POST['fullname'];
	$email		= $_POST['email'];
	$password 	= sha1($_POST['password']);
	$middlestrt = strtotime($_POST['dob']);        
	$date 		= date('Y-m-d', $middlestrt); 

	if (empty($fullname) || empty($email) || empty($password) || empty($date)) {
		echo '<div class="alert alert-danger" style="font-size: 15px; text-align: center;">
	   			<i class="fa fa-times-circle"></i> A field is empty. Please enter all data.
	      	</div>';

	} elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		echo '<div class="alert alert-danger" style="font-size: 15px; text-align: center;">
	   			<i class="fa fa-times-circle"></i> Invalid email format.
	      	</div>';

	} else {
	
		$insert_data = array(
			"fullname" 	=> mysqli_real_escape_string($data->conn, $fullname),
			"email"		=> mysqli_real_escape_string($data->conn, $email),
			"password"	=> mysqli_real_escape_string($data->conn, $password),
			"dob"		=> $date,
			//"status"	=> 1
		);

		$result = $data->insert('tbl_user_detail', $insert_data);

		$sqlcheck = $data->select("email", "`tbl_user_detail`", "`email` = ?"); 
		$stmt = $mysqli->prepare($sqlcheck);
	    $stmt->bind_param('s', $email);
	    $stmt->execute();

	    $resultcheck = $stmt->get_result();
		

		if($row = $resultcheck->fetch_assoc()){

			echo '<script> 
					swal({
						title: "User already exists!",
						icon: "warning",
						showCancelButton: true,
						cancelButtonText: "Cancel",

					});
				</script>';
			
		} else {

			if ($result) {
				
				echo '<script> 
						swal({
						  title: "User Registered Successfully!",  
						  icon: "success",
						  button: "Ok",
						});
					</script>';
				
			} else {

				echo '<div class="alert alert-danger" style="font-size: 15px; text-align: center;">
			   			<i class="fa fa-times-circle"></i> Invalid Registration 
			      	</div>';
			}
			
		}

	}

	
    

?>