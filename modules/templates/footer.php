</body>

</html>

<!-- Footer -->
<footer>
    <div class="container-fluid footersection">
        <div class="row">        
            <div class="col-md-12 copysection">
                <p class="copyright">Copyright &copy; Rikesh Upadhyay (<i>c7171819</i>) 2017</p>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</footer>



<script src="../../vendor/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../vendor/js/bootstrap.min.js"></script>

<!-- Angular JS -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

<script src="../../vendor/js/jquery.validate.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- Sweet Alert JS -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


