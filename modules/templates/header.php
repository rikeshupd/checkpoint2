<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title; ?></title>

    <!-- British College Icon -->
    <link rel="icon" href="../../public/image/icon/lbu_FLu_icon.ico">

    <!-- Bootstrap Core CSS -->
    <link href="../../vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../vendor/css/custom.css" rel="stylesheet">


    <!-- Custom fonts for this template -->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Custom CSS -->
    <!-- <link href="../../vendor/css/half-slider.css" rel="stylesheet"> -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="mainNav">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">CHECKPOINT - 2</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->

            <?php if ( (!isset($_SESSION["email"])) && (!isset($_SESSION["userid"])) )  : ?>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="angularjs.php">AngularJS</a>
                        </li>
                        <li>
                            <a href="jsonxml.php">JSON/XML</a>
                        </li>
                        <li>
                            <a href="contact.php">Contact</a>
                        </li>
                        <li>
                            <a href="login.php">Login</a>
                        </li>
                        <li>
                            <a href="signup.php">Signup</a>
                        </li>
                    </ul>
                </div>

            <?php elseif (isset($_SESSION["email"]) && isset($_SESSION["userid"])) : ?>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                         <li>
                            <a href="angularjs.php">AngularJS</a>
                        </li>
                        <li>
                            <a href="jsonxml.php">JSON/XML</a>
                        </li>
                        <li>
                            <a href="contact.php">Contact</a>
                        </li>
                        <li class="dropdown welcome">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i><?= $_SESSION['welcome'].' ';?>&nabla;</a>

                            <ul class="dropdown-menu welcomemenu" style="width: 100%">
                               <!--  <li class="menu-item"><a href="../views/profile.php" class="sub-account"> Profile </a></li>
                                <li class="menu-item"><a href="../views/eventjoined.php" class="sub-account"> Events Joined </a></li> -->
                                <li class="menu-item"><a href="../control/logoutaction.php" class="sub-account"> Logout </a></li>
                            </ul>
                        </li>
                       
                    </ul>
                </div>
            <?php endif;?>

            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>