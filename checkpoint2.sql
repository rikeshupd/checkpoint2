-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2018 at 04:47 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `checkpoint2`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_angular`
--

CREATE TABLE `tbl_angular` (
  `id` int(10) NOT NULL,
  `name` varchar(30) NOT NULL,
  `groupname` varchar(30) NOT NULL,
  `number` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_angular`
--

INSERT INTO `tbl_angular` (`id`, `name`, `groupname`, `number`) VALUES
(1, 'Hishan Amatya', 'BSc', 2),
(2, 'Rikesh Upadhyay', 'Bsc', 3),
(3, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_json_xml`
--

CREATE TABLE `tbl_json_xml` (
  `id` int(10) NOT NULL,
  `country` varchar(30) NOT NULL,
  `capital` varchar(30) NOT NULL,
  `longitude` varchar(10) NOT NULL,
  `latitude` varchar(10) NOT NULL,
  `currency` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_json_xml`
--

INSERT INTO `tbl_json_xml` (`id`, `country`, `capital`, `longitude`, `latitude`, `currency`) VALUES
(1, 'Nepal', 'Kathmandu', '28.3949', '84.1240', 'Nepali rupee'),
(2, 'China', 'Beijing', '116.363625', '39.913818', 'Renminbi'),
(3, 'Japan', 'Tokyo', '36.2048', '138.252', 'Yen'),
(4, 'India', 'New Delhi', '20.5937', '78.9629', 'Indian rupee'),
(5, 'USA', 'Washington, D.C.', '37.0902', '95.7129', 'US Dollar');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_detail`
--

CREATE TABLE `tbl_user_detail` (
  `id` int(10) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(150) NOT NULL,
  `dob` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_detail`
--

INSERT INTO `tbl_user_detail` (`id`, `fullname`, `email`, `password`, `dob`) VALUES
(1, 'Rikesh', 'rikeshsemail@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', '1997-10-10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_angular`
--
ALTER TABLE `tbl_angular`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_json_xml`
--
ALTER TABLE `tbl_json_xml`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_detail`
--
ALTER TABLE `tbl_user_detail`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_angular`
--
ALTER TABLE `tbl_angular`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_json_xml`
--
ALTER TABLE `tbl_json_xml`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_user_detail`
--
ALTER TABLE `tbl_user_detail`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
