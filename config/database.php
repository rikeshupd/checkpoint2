<?php

class Database {
	
	protected $db_name = 'checkpoint2';
	protected $db_user = 'root';
	protected $db_pass = '';
	protected $db_host = 'localhost';
	public $conn;
	
	// Open a connect to the database.
	// Make sure this is called on every page that needs to use the database.

	public function __construct()
	{
		$this->conn = new mysqli( $this->db_host, $this->db_user, $this->db_pass, $this->db_name );
		
		if ( mysqli_connect_errno() ) {
			printf("Connection failed: %s", mysqli_connect_error());
			exit;
		}
		//echo "success";
		return true;
	}

	
	public function insert($table_name, $data) 
	{
		$sql = "INSERT INTO ".$table_name." (";
		$sql .= implode(",", array_keys($data)) . ') VALUES (';
		$sql .= "'" . implode("','", array_values($data)) . "')";

		$result = mysqli_query($this->conn, $sql);

		if ($result) {
			return true;
		} else {
			echo mysqli_error($this->conn);
		}
	}

	public function select($columns = ' * ', $tableName = "", $criteria = "", $clause = "")
	{
		if (empty($tableName)) return false;

        $sql = " SELECT " . $columns . " FROM " . $tableName;
        if (!empty($criteria)) {
            $sql .= " WHERE " . $criteria;
        }

        if (!empty($clause)) {
            $sql .= ' ' . $clause;
        }

        return $sql;

        // $result = mysqli_query($this->conn, $sql);
        // $row = mysqli_fetch_assoc($result);
        // while ($row) {
        // 	$array[] = $row;
        // }
        // return $array;

	}


	public function update($table_name, $fields, $where_condition)
	{
		$query = '';
		$condition = '';

		foreach ($fields as $key => $value) 
		{
			$query .= $key . "='" . $value. "', ";
		}

		$query = substr($query, 0, -2);

		foreach ($where_condition as $key => $value) 
		{
			$condition .= $key . "='". $value . "' AND ";
		}

		$condition = substr($condition, 0, -5);

		$query = "UPDATE ".$table_name." SET ".$query." WHERE ".$condition."";

		return $query;  
	}


	public function delete($tableName, $criteria = "")
    {
        if (empty($tableName) && empty($criteria)) return false;
        $sql = " DELETE FROM " . $tableName . " WHERE " . $criteria;
        
        return $sql;
    }



	
}

// return $db = new Database();
// return $db->connect();  